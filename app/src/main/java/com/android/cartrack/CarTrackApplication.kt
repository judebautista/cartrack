package com.android.cartrack

import android.app.Application
import com.android.cartrack.data.network.NetworkConnectionInterceptor
import com.android.cartrack.data.network.NetworkModule
import com.android.cartrack.data.room.AppDatabase
import com.android.cartrack.repository.DatabaseRepository
import com.android.cartrack.repository.NetworkRepostory
import com.android.cartrack.ui.home.factory.HomeViewModelFactory
import com.android.cartrack.ui.login.factory.LoginViewModelFactory
import com.android.cartrack.ui.register.factory.RegisterViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class CarTrackApplication : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@CarTrackApplication))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { NetworkModule(instance()) }
        bind() from singleton { AppDatabase(instance()) }
        bind() from singleton { NetworkRepostory(instance()) }
        bind() from singleton { DatabaseRepository(instance()) }
        bind() from provider { LoginViewModelFactory(DatabaseRepository(instance())) }
        bind() from provider { RegisterViewModelFactory(DatabaseRepository(instance())) }
        bind() from provider { HomeViewModelFactory(NetworkRepostory(instance())) }
    }


}