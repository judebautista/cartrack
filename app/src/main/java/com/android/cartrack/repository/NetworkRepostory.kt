package com.android.cartrack.repository

import com.android.cartrack.data.network.NetworkModule
import com.android.cartrack.data.network.SafeApiRequest
import com.android.cartrack.ui.home.model.UserResponse


class NetworkRepostory
    (
    private val api: NetworkModule
) : SafeApiRequest() {

    suspend fun login(): UserResponse {
        return apiRequest {
            api.service().login()
        }
    }


}