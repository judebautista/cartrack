package com.android.cartrack.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.android.cartrack.data.room.AppDatabase
import com.android.cartrack.data.room.entities.UserInfo

class DatabaseRepository(private val db: AppDatabase) {
    suspend fun registerUser(country: String,username: String, password: String):Long {
       return db.userInfoDao().insert(UserInfo(country,username, password))
    }

      fun getUser(country:String,username: String,password: String):Boolean {
        return db.userInfoDao().getUser(country,username,password)
    }

}