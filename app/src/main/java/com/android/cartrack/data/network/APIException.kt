package com.android.cartrack.data.network

import java.io.IOException

class APIException(message:String):IOException(message)
class NoNetworkException(message:String):IOException(message)