package com.android.cartrack.data.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.cartrack.data.room.entities.UserInfo

@Dao
interface UserInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(userInfo: UserInfo): Long

    @Query("SELECT * FROM UserInfo where country = :county and username = :username and password = :password ")
     fun getUser(county:String,username: String,password:String): Boolean
}