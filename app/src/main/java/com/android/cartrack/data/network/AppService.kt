package com.android.cartrack.data.network

import com.android.cartrack.ui.home.model.UserResponse
import retrofit2.Response
import retrofit2.http.GET

interface AppService {

    @GET("users")
    suspend fun login() : Response<UserResponse>


}