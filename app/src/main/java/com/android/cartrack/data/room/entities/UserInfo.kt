package com.android.cartrack.data.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class UserInfo(
    @ColumnInfo(name = "country") var country: String,
    @ColumnInfo(name = "username") var username: String,
    @ColumnInfo(name = "password") var password: String
)
{
    @PrimaryKey(autoGenerate = true)
    var userID: Int = 0
}
