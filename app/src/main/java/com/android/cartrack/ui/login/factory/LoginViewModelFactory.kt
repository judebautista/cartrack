package com.android.cartrack.ui.login.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.cartrack.repository.DatabaseRepository
import com.android.cartrack.repository.NetworkRepostory
import com.android.cartrack.ui.login.viewModel.LoginViewModel

@Suppress("UNCHECKED_CAST")
class LoginViewModelFactory(
    private val databaseRepository: DatabaseRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return LoginViewModel(databaseRepository) as T
    }
}