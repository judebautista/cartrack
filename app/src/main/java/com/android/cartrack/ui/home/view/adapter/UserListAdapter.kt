package com.android.cartrack.ui.home.view.adapter

import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.android.cartrack.R
import com.android.cartrack.ui.home.model.UserResponse
import com.android.cartrack.utils.BaseRecyclerViewAdapter
import com.android.cartrack.utils.inflate
import kotlinx.android.synthetic.main.item_user.view.*

class UserListAdapter(override var items: MutableList<UserResponse.details>) :
    BaseRecyclerViewAdapter<UserResponse.details>(items) {
    private var itemAction: ((UserResponse.details) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_user))
    }


    override fun onBindViewHolder(view: View, item: UserResponse.details, position: Int) {
        with(view)
        {
            if (position % 3 == 0) {
                cardViewBg.setCardBackgroundColor(ContextCompat.getColor(context, R.color.green))
            } else {
                if (position % 2 == 0) {
                    cardViewBg.setCardBackgroundColor(ContextCompat.getColor(context, R.color.red))
                } else {
                    cardViewBg.setCardBackgroundColor(
                        ContextCompat.getColor(
                            context,
                            R.color.yellow
                        )
                    )
                }
            }

            name.text = item.name
            email.text = item.email
            cardViewBg.setOnClickListener {
                itemAction!!.invoke(item)
            }
        }


    }

    fun setItemAction(action: (UserResponse.details) -> Unit) {
        this.itemAction = action
    }

    fun setNewItems(newItems: List<UserResponse.details>) {
        this.items.clear()
        this.items.addAll(newItems)
        notifyDataSetChanged()
    }
}