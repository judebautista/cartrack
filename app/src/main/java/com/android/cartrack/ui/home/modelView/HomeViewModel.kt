package com.android.cartrack.ui.home.modelView

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.cartrack.data.coroutines.Coroutines
import com.android.cartrack.data.network.APIException
import com.android.cartrack.data.network.NoNetworkException
import com.android.cartrack.repository.NetworkRepostory
import com.android.cartrack.ui.home.model.UserResponse


class HomeViewModel(
    private val networkRepostory: NetworkRepostory
) : ViewModel() {


    var showMessage = MutableLiveData<String>()
    var loading = MutableLiveData<Boolean>()
    var userResponse = MutableLiveData<UserResponse>()

    init {
        loading.value = false
    }

    fun getUserDetails() {
        loading.value = true
        Coroutines.main {
            try {
                val response = networkRepostory.login()
                response?.let {
                    loading.value = false
                    userResponse.value = response
                    return@main
                }
            } catch (e: APIException) {
                loading.value = false
                showMessage.value = e.message
            } catch (e: NoNetworkException) {
                loading.value = false
                showMessage.value = e.message
            }

        }

    }
}