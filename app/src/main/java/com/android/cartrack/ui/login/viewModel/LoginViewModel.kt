package com.android.cartrack.ui.login.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.cartrack.repository.DatabaseRepository


class LoginViewModel(
    private val databaseRepository: DatabaseRepository
) : ViewModel() {

    var showMessage = MutableLiveData<String>()
    var successLogin = MutableLiveData<Boolean>()


    fun loginBtn(country: String, username: String, password: String) {

        if (country.isNullOrEmpty()) {
            showMessage.value = "Please select your country"
            return
        }

        if (username.isNullOrEmpty() || password.isNullOrEmpty()) {
            showMessage.value = "Invalid username or password"
            return
        }
        val result = databaseRepository.getUser(country,username, password)
        if (result) {
            successLogin.value = result
        } else {
            showMessage.value = "User account does not exist in database"
        }


    }


}
