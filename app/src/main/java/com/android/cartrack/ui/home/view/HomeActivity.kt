package com.android.cartrack.ui.home.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.cartrack.R
import com.android.cartrack.ui.home.factory.HomeViewModelFactory
import com.android.cartrack.ui.home.modelView.HomeViewModel
import com.android.cartrack.ui.home.view.adapter.UserListAdapter
import com.android.cartrack.ui.user.UserLocationDetailsActivity
import com.android.cartrack.utils.hide
import com.android.cartrack.utils.show
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_home.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class HomeActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory: HomeViewModelFactory by instance()
    lateinit var userListAdapter: UserListAdapter
    lateinit var viewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        viewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)

        init()
    }


    fun init() {
        viewModel.getUserDetails()

        viewModel.loading.observe(this, Observer {
            showProgressBar(it)
        })


        setUserList()

    }

    fun setUserList() {

        userList_rv.layoutManager = LinearLayoutManager(this)
        userListAdapter = UserListAdapter(mutableListOf())
        userList_rv.adapter = userListAdapter

        viewModel.userResponse.observe(this, Observer {
            userListAdapter.setNewItems(it)
        })
        userListAdapter.setItemAction {
            val intent = Intent(baseContext, UserLocationDetailsActivity::class.java)
            var jsonString = Gson().toJson(it)
            intent.putExtra("userInfo", jsonString)
            startActivity(intent)
        }
    }


    fun showProgressBar(show: Boolean) {
        if (show) {
            loading_pb.show()
        } else {
            loading_pb.hide()
        }
    }

}