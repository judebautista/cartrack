package com.android.cartrack.ui.home.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.cartrack.repository.DatabaseRepository
import com.android.cartrack.repository.NetworkRepostory
import com.android.cartrack.ui.home.modelView.HomeViewModel
import com.android.cartrack.ui.login.viewModel.LoginViewModel

@Suppress("UNCHECKED_CAST")
class HomeViewModelFactory(
    private val networkRepostory: NetworkRepostory
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeViewModel(networkRepostory) as T
    }
}