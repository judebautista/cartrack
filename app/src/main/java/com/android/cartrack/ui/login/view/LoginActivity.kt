package com.android.cartrack.ui.login.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.android.cartrack.R
import com.android.cartrack.ui.home.view.HomeActivity
import com.android.cartrack.ui.login.factory.LoginViewModelFactory
import com.android.cartrack.ui.login.viewModel.LoginViewModel
import com.android.cartrack.ui.register.view.RegisterActivity
import com.android.cartrack.utils.toast
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.country_sp
import kotlinx.android.synthetic.main.activity_login.password_et
import kotlinx.android.synthetic.main.activity_login.username_et
import kotlinx.android.synthetic.main.activity_register.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class LoginActivity : AppCompatActivity(), View.OnClickListener, KodeinAware {

    override val kodein by kodein()
    private val factory: LoginViewModelFactory by instance()
    var country : String = ""
    lateinit var viewModel: LoginViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        viewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)

        init()
    }

    fun init() {
        loginBtn.setOnClickListener(this)
        registerBtn.setOnClickListener(this)

        setUpCountry()

        viewModel.showMessage.observe(this, Observer {
            toast(it)
        })

        viewModel.successLogin.observe(this, Observer {
            if (it) {
                startActivity(Intent(this, HomeActivity::class.java))
                finish()
            }
        })

    }


    private fun setUpCountry() {
        ArrayAdapter.createFromResource(this, R.array.country, android.R.layout.simple_spinner_item)
            .also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                country_sp.adapter = adapter
            }
        country_sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                if (pos != 0)
                    country = parent.getItemAtPosition(pos).toString()

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
    }


    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.registerBtn -> startActivity(
                Intent(
                    this,
                    RegisterActivity::class.java
                )
            )

            R.id.loginBtn -> viewModel.loginBtn(
                country,
                username_et.text.trim().toString(),
                password_et.text.trim().toString()
            )

        }

    }


}


