package com.android.cartrack.ui.user

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.android.cartrack.R
import com.android.cartrack.ui.home.model.UserResponse
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_user_location.*


class UserLocationDetailsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var details: UserResponse.details
    var userAddress: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_location)


        val strObj = intent.getStringExtra("userInfo")
        details = Gson().fromJson(strObj, UserResponse.details::class.java)


        init()
    }

    fun init() {
        setupMap()
        var detailsAddress = details.address
        userAddress =
            "${detailsAddress.street} ${detailsAddress.suite} , ${detailsAddress.city} zipCode: ${detailsAddress.zipcode} "
        address.text = userAddress

        phoneNumber.text = details.phone

        company.text =
            "${details.company.name}, \n ${details.company.catchPhrase},\n ${details.company.bs}"

    }

    fun setupMap() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setMinZoomPreference(6.0f)
        mMap.setMaxZoomPreference(14.0f)
        mMap.uiSettings.isZoomControlsEnabled = true;
        var detailsAddress = details.address
        val locationUser =
            LatLng(detailsAddress.geo.lat.toDouble(), detailsAddress.geo.lat.toDouble())
        mMap.addMarker(
            MarkerOptions().position(locationUser)
                .title(userAddress)
        )
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationUser, 12.0f))
    }
}