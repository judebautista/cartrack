package com.android.cartrack.ui.register.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.cartrack.repository.DatabaseRepository
import com.android.cartrack.repository.NetworkRepostory
import com.android.cartrack.ui.login.viewModel.LoginViewModel
import com.android.cartrack.ui.register.viewModel.RegisterViewModel

@Suppress("UNCHECKED_CAST")
class RegisterViewModelFactory(
    private val databaseRepository: DatabaseRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RegisterViewModel(databaseRepository) as T
    }
}