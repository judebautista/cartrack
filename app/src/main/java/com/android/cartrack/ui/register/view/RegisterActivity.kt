package com.android.cartrack.ui.register.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.cartrack.R
import com.android.cartrack.ui.home.view.adapter.UserListAdapter
import com.android.cartrack.ui.login.factory.LoginViewModelFactory
import com.android.cartrack.ui.register.factory.RegisterViewModelFactory
import com.android.cartrack.ui.register.viewModel.RegisterViewModel
import com.android.cartrack.utils.snackbar
import com.android.cartrack.utils.toast
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.country_sp
import kotlinx.android.synthetic.main.activity_register.password_et
import kotlinx.android.synthetic.main.activity_register.root_layout
import kotlinx.android.synthetic.main.activity_register.username_et
import  org.kodein.di.android.kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance

class RegisterActivity : AppCompatActivity(), KodeinAware, View.OnClickListener {

    override val kodein by kodein()
    private val factory: RegisterViewModelFactory by instance()
    lateinit var country:String
    lateinit var viewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        viewModel = ViewModelProvider(this, factory).get(RegisterViewModel::class.java)
        init()
    }

    fun init() {
        signUpBtn.setOnClickListener(this)
        back.setOnClickListener(this)
        viewModel.message.observe(this, Observer {
            root_layout.snackbar(it, "Ok")
        })
        viewModel.successRegister.observe(this, Observer {
            if (it) {
                toast("Successful Register")
                finish()
            }
        })

        setUpCountry()
    }


    private fun setUpCountry() {
        ArrayAdapter.createFromResource(this, R.array.country, android.R.layout.simple_spinner_item)
            .also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                country_sp.adapter = adapter
            }
        country_sp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                if (pos != 0)
                    country = parent.getItemAtPosition(pos).toString()

            }
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
    }

    override fun onClick(view: View?) {

        when (view!!.id) {
            R.id.signUpBtn -> viewModel.userRegister(
                country,
                username_et.text.trim().toString(),
                password_et.text.trim().toString(),
                password_confirm_et.text.trim().toString()
            )
            R.id.back -> finish()
        }
    }


}