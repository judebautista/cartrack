package com.android.cartrack.ui.register.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.cartrack.data.coroutines.Coroutines
import com.android.cartrack.repository.DatabaseRepository

class RegisterViewModel(private var db: DatabaseRepository) : ViewModel() {
    var message = MutableLiveData<String>()
    var successRegister = MutableLiveData<Boolean>()
    init {
        successRegister.value = false
    }

    fun userRegister(country:String,username: String, password: String, confirmPassword: String) {
        if (username.isNullOrEmpty() || password.isNullOrEmpty() || confirmPassword.isNullOrEmpty() || country.isNullOrEmpty()) {
            message.value = "Please fill up your signup"
            return
        } else if (password != confirmPassword) {
            message.value = "Your password and confirm password did not match"
        } else {
            Coroutines.main {
                var result = db.registerUser(country,username, password)
                if (result != 0L) {
                    successRegister.value = true
                }
            }

        }
    }

}